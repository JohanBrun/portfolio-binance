import React from 'react';
import './App.css';
import { Charts } from './Components/Charts';
import { Inputs } from './Components/Inputs';

const App : React.FC = (props)=> {
  const [beginning, setBeginning] = React.useState('2021-01-01')
  const [end, setEnd] = React.useState('2021-12-01')
  const [btcValue, setBtcValue] = React.useState(1)
  const [ethValue, setEthValue] = React.useState(1)
  const [solValue, setSolValue] = React.useState(1)
  const [lunaValue, setLunaValue] = React.useState(1)


  return (
    <div className="App">
      <Charts btcValue={btcValue} ethValue={ethValue} solValue={solValue} lunaValue={lunaValue} beginning={beginning} end={end} />
      <Inputs beginning={beginning} 
              setBeginning={setBeginning} 
              end={end} 
              setEnd={setEnd} 
              btcValue={btcValue} 
              setBtcValue={setBtcValue} 
              ethValue={ethValue} 
              setEthValue={setEthValue} 
              solValue={solValue} 
              setSolValue={setSolValue} 
              lunaValue={lunaValue} 
              setLunaValue={setLunaValue} />
    </div>
  );
}

export default App;
