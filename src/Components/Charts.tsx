import axios from 'axios';
import * as React from 'react'
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, ReferenceLine } from 'recharts'

interface iCharts {
    btcValue: number,
    ethValue: number,
    solValue: number,
    lunaValue: number,
    beginning: string,
    end: string,
}

interface iCrypto {
    name: string,
    value: number,
}

export const Charts: React.FC<iCharts> = (props) => {

    const [cryptosData, setCryptosData] = React.useState<Array<any>>([])

    const getCrypto = (crypto: string, cryptoValue: number, tempDataArray: Array<any>, startDate: string, endDate: string) => {
        return axios({
            method: 'get',
            url: `https://data.messari.io/api/v1/assets/${crypto}/metrics/price/time-series`,
            params: {
                start: startDate,
                end: endDate,
                interval: "1d",
                columns: "close",
                format: "json",
            }
        }).then((response)=>{
            if(tempDataArray.length === 0){
                tempDataArray = response.data.data.values.map((value: any)=>{
                    return {
                        day: value[0],
                        [crypto]: value[1]*cryptoValue
                    }
                })
            }
            else{
                for(const value of response.data.data.values){
                    let dayData = tempDataArray.find(element => element.day === value[0])
                    if(dayData !== undefined){
                        dayData[crypto] = value[1]*cryptoValue
                        tempDataArray = tempDataArray.map((element)=>{
                           return element.day !== dayData.day ? element : dayData 
                        })
                    }
                }
            }
            return tempDataArray
        }).catch((error)=>{
            console.log(error)
            return tempDataArray
        })
    }

    const getCryptosData = async (cryptoList: Array<iCrypto>, startDate: string, endDate: string) => {
        let tempDataArray: Array<any> = []
        for(const crypto of cryptoList){
            tempDataArray = await getCrypto(crypto.name, crypto.value, tempDataArray, startDate, endDate)
        }
        return tempDataArray
    }

    const getReadableDate = (tempDataArray: Array<any>) => {
        let readableDateArray: Array<any> = []
        for(const dayData of tempDataArray){
            let day = new Date(dayData.day)
            dayData.day = `${('0'+ day.getUTCDate()).slice(-2)}-${('0'+ (day.getUTCMonth()+1)).slice(-2)}-${day.getUTCFullYear()}`
            readableDateArray.push(dayData)
        }
        return readableDateArray
    }

    React.useEffect(()=>{
        getCryptosData([{name: 'BTC', value: props.btcValue}, {name: 'ETH', value: props.ethValue}, {name: 'SOL', value: props.solValue}, {name: 'LUNA', value: props.lunaValue}], props.beginning, props.end).then((tempDataArray) => {
            let dataArray = getReadableDate(tempDataArray)
            setCryptosData(dataArray)
        })
    },[props.beginning, props.end, props.btcValue, props.ethValue, props.solValue, props.lunaValue])

    let maxValue = 0
    let minValue = Number.MAX_VALUE
    for(let day of cryptosData) {
        let sum = day.BTC+day.ETH+day.SOL+day.LUNA
        if(maxValue < sum){
            maxValue = sum
        }
        if(minValue > sum){
            minValue = sum
        }
    }

    return(
        <div>
            <ResponsiveContainer width="50%" height={600}>
                <AreaChart
                    width={500}
                    height={400}
                    data={cryptosData}
                    margin={{
                        top: 10,
                        right: 30,
                        left: 0,
                        bottom: 0,
                    }}
                >
                    <CartesianGrid strokeDasharray="5 5" />
                    <XAxis dataKey="day" />
                    <YAxis />
                    <Tooltip />
                    <Area type="monotone" dataKey="BTC" stackId="1" stroke="#8884d8" fill="#8884d8" />
                    <Area type="monotone" dataKey="ETH" stackId="1" stroke="#82ca9d" fill="#82ca9d" />
                    <Area type="monotone" dataKey="SOL" stackId="1" stroke="#ffc658" fill="#ffc658" />
                    <Area type="monotone" dataKey="LUNA" stackId="1" stroke="#339fff" fill="#339fff" />
                    <ReferenceLine y={maxValue} label={`Max = $${maxValue.toFixed(2)}`} stroke="green" strokeDasharray="3 3" />
                    <ReferenceLine y={minValue} label={`Min = $${minValue.toFixed(2)}`} stroke="red" strokeDasharray="3 3" />
                </AreaChart>
            </ResponsiveContainer>
        </div>
    )
}