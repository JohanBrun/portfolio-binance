import * as React from 'react'

interface iInputs {
    beginning: string,
    setBeginning: any,
    end: string,
    setEnd: any,
    btcValue: number,
    setBtcValue: any,
    ethValue: number,
    setEthValue: any,
    solValue: number,
    setSolValue: any,
    lunaValue: number,
    setLunaValue: any
}

export const Inputs: React.FC<iInputs> = (props: any) => {

    const handleSubmit = (event: any) => {
        event.preventDefault()
        props.setBeginning(event.target.beginning.value)
        props.setEnd(event.target.end.value)
        props.setBtcValue(event.target.BTCvalue.value)
        props.setEthValue(event.target.ETHvalue.value)
        props.setSolValue(event.target.SOLvalue.value)
        props.setLunaValue(event.target.LUNAvalue.value)
    }

    return(
        <div>
            <form onSubmit={handleSubmit}>
                <label>Date début
                    <input type="date" name="beginning" defaultValue={props.beginning} />
                </label>
                <label>Date fin
                    <input type="date" name="end" defaultValue={props.end} />
                </label>
                <label>BTC
                    <input type="number" step="0.00000001" name="BTCvalue" defaultValue={props.btcValue} />
                </label>
                <label>ETH
                    <input type="number" step="0.00000001"name="ETHvalue" defaultValue={props.ethValue} />
                </label>
                <label>SOL
                    <input type="number" step="0.00000001" name="SOLvalue" defaultValue={props.solValue} />
                </label>
                <label>LUNA
                    <input type="number" step="0.00000001" name="LUNAvalue" defaultValue={props.lunaValue} />
                </label>
                <input type="submit" value="enregistrer" />
                
            </form>
            
        </div>
    )
}